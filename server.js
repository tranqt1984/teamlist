const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 2000;
const db = require('./db.json');

app.use(express.static('build'))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/build/index.html')
})


app.get('/todos', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.json(db);
})

app.post('/todos', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  db.push({"id": req.body.id, "name": req.body.name});
  res.json(db);
})

app.all('*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  next();
})


app.listen(port, () => console.log(`Server live on port ${port}`));