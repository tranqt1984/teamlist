import React, { useState, useEffect } from 'react';
import './App.scss';

function App() {
  const [list, setList] = useState([]);
  const [dataId, setDataId] = useState(2);
  const [dataName, setDataName] = useState("");

  useEffect(() => {
    fetch('http://localhost:2000/todos')
      .then(res => res.json())
      .then(json => setList(json))
  }, []);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({id: dataId.toString(), name: dataName})
    }
    setDataId(dataId + 1);

    const res = await fetch('http://localhost:2000/todos', options);
    const json = await res.json();
    setList(json)

    setDataName("");
  }

  const handleInputchange = (e) => {
    setDataName(e.target.value)
  }

  return (
    <div>
      <h1>Add people to your team!</h1>
      <form onSubmit={handleFormSubmit}>
        <input type="text" onChange={handleInputchange} placeholder="Add first and last name..." value={dataName}/>
        <button>Submit</button>
      </form>
      <ul>{list.map(todo => <li key={todo.id}>{todo.name}</li>)}</ul>
    </div>
  );
}

export default App;
